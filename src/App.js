import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { IoCall } from 'react-icons/io5'
import { FaCaretUp } from 'react-icons/fa';

import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Varieties from './components/Varieties';
import Limited from './components/Limited';
import Pricing from './components/Pricing';
import Builder from './components/Builder';

import PricingGold from './components/PricingGold';
import PricingSilver from './components/PricingSilver';
import PricingBasic from './components/PricingBasic';

import Gallery from './components/Gallery';
import Footer from './components/Footer';

import './App.css'

class App extends React.Component {
  state = {
    showFab: false
  }

  constructor(props) {
    super(props);

    this.state = {
      showFab: false
    }

    setTimeout(() => {
      this.onRouteChange(props.location, '');
    }, 800);

    props.history.listen((data) => {
      setTimeout(() => {
        this.onRouteChange(data, props.history);
      }, 500);
    });
  }

  onRouteChange(data, history) {
    if (history.action === "REPLACE") return;
    let element;

    switch(data.pathname) {
      case '/':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
          showFab: false
        });
        break;
      case '/varieties':
        element = document.getElementById('varieties')
        this.setState({
          showFab: false
        });
        break;
      case '/pricing':
        element = document.getElementById('pricing')
        this.setState({
          showFab: false
        });
        break;
      case '/builder':
        element = document.getElementById('builder')
        this.setState({
          showFab: false
        });
        break;
      case '/pricing/gold':
        element = document.getElementById('pricing-ques')
        this.setState({
          showFab: false
        });
        break;
      case '/pricing/silver':
        element = document.getElementById('pricing-ques')
        this.setState({
          showFab: false
        });
        break;
      case '/pricing/bronze':
        element = document.getElementById('pricing-ques')
        this.setState({
          showFab: false
        });
        break;
      case '/gallery':
        element = document.getElementById('gallery')
        this.setState({
          showFab: false
        });
        break;
      case '/contact':
        element = document.getElementById('footer')
        this.setState({
          showFab: false
        });
        break;
      default:
        this.setState({
          showFab: false,
          location: data
        });
        break;
    }

    if (!element) return;
    element.scrollIntoView({behavior: 'smooth'});
  }

  componentDidMount() {
    document.addEventListener('scroll', (e) => {
      this.setState({
        showFab: window.scrollY > 86
      })
    });
  }

  BackToTop() {
    document.getElementById('landing').scrollIntoView({behavior: 'smooth'});
  }

  render() {
    return (
      <React.Fragment>
        <Landing />
        <Varieties />
        <Pricing />
        <Builder />
        <Route exact path="/pricing/bronze">
          <PricingBasic />
        </Route>
        <Route exact path="/pricing/silver">
          <PricingSilver />
        </Route>
        <Route exact path="/pricing/gold">
          <PricingGold />
        </Route>
        <Gallery />
        <Footer />
        <div className={"fab flexible center popup primaryFill left show"}>
          <a href="tel:6193723153"></a>
          <IoCall className="tertiary" />
        </div>
        <div onClick={this.BackToTop} className={"fab flexible center popup primaryFill bckTTp" + (this.state.showFab ? ' show' : '')}>
          <FaCaretUp className="tertiary" />
        </div>
      </React.Fragment>
    )
  }
}

export default withRouter(App);
