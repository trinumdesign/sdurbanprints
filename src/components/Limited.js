import { NavLink } from 'react-router-dom';

function Limited() {
  const pictures = [
    "150976156_187659176244163_1562158552957803313_n_ko3h7y",
    "29495837_1695748877138741_5099193196257214464_o_d0ibnc",
    "41556893_1932454503468176_6570569719655432192_o_t47azi",
    "54372158_2204128826300741_8243628969446866944_n_ttplxu",
    "80848313_2742822305764721_1518241492840218624_o_fo72hm",
    "27164242_1639136586133304_7468093547981958187_o_cmkimg",
    "22498942_1538300342883596_433721800749659861_o_u9holr",
    "20157392_1452271791486452_1111839122480524856_o_ygcjjk",
    "38201179_1868507033196257_3108495641475547136_n_r4pdtu",
    "15977367_1253299574717009_4188394884920915142_n_js6qmj",
    "23000049_1551331854913778_4842756238256257432_o_efgvav",
    "15965574_1253299594717007_4917088405044767441_n_s7ylov",
    "15894533_1253260061387627_5371627676498189585_n_cjtk13"
  ];

  return (
    <>
      <div id="limited" className="twentyfv-height primaryFill flexible full-width center columns">
        <div className="break"></div>
        <div className="break"></div>
        <h4 className="flexible center full-width tertiary">Limited Time Offer</h4>
        <div className="break"></div>
        <h1 className="flexible center tertiary text-center">Reorder Within The Next 4 Months and Get 50% OFF The Setup Fee</h1>
        <div className="break"></div>
        <div className="break"></div>
      </div>
    </>
  )
}

export default Limited;
