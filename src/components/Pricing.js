import { NavLink } from 'react-router-dom';
import Socials from './Socials';

import '../styles/Pricing.css'

function Pricing() {
  return (
    <>
      <div id="pricing" className="naityfv-height bg-color flexible full-width center columns">
        <div className="break"></div>
        <div className="break"></div>
        <h4 className="flexible center full-width primary tertiary-dark">Our Pricing</h4>
        <div className="break"></div>
        <h1 className="flexible center tertiary-dark text-center">Check Out Our Pricing Models</h1>
        <div className="break"></div>
        <div className="break"></div>
        <div className="flexible full-width rows center wrap">
          <div className="model-c flexible center">
            <div className="m-cont flexible columns center">
              <h1 className="primary full-width text-center">Bronze</h1>
              <div className="break"></div>
              <h4 className="tertiary-dark full-width text-center">from</h4>
              <h1 className="tertiary-dark full-width text-center m-c-p">$2.50</h1>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <p className="tertiary-dark">24 - 71 Pieces</p>
              <div className="divider"></div>
              <p className="tertiary-dark">2 Week Fulfillment</p>
              <div className="divider"></div>
              <p className="tertiary-dark">Perfect for Small Teams</p>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <div className="flexible center full-width">
                <button className="flexible center tertiary clickable">
                  <NavLink to="/pricing/bronze"></NavLink>
                  Order Now
                </button>
              </div>
            </div>
          </div>
          <div className="model-c flexible center">
            <div className="m-cont b flexible columns center">
              <h1 className="tertiary full-width text-center">Gold</h1>
              <div className="break"></div>
              <h4 className="tertiary full-width text-center">from</h4>
              <h1 className="tertiary full-width text-center m-c-p">$1.75</h1>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <p className="tertiary">144+ Pieces</p>
              <div className="divider"></div>
              <p className="tertiary">2.5 Week Fulfillment</p>
              <div className="divider"></div>
              <p className="tertiary text-center">Perfect for Large Scaled Businesses</p>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <div className="flexible center full-width">
                <button className="flexible center primary clickable tertiaryFill">
                  <NavLink to="/pricing/gold"></NavLink>
                  Order Now
                </button>
              </div>
            </div>
          </div>
          <div className="model-c flexible center">
            <div className="m-cont flexible columns center">
              <h1 className="primary full-width text-center">Silver</h1>
              <div className="break"></div>
              <h4 className="tertiary-dark full-width text-center">from</h4>
              <h1 className="tertiary-dark full-width text-center m-c-p">$2.00</h1>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <p className="tertiary-dark">72 - 143 Pieces</p>
              <div className="divider"></div>
              <p className="tertiary-dark">2.5 Week Fulfillment</p>
              <div className="divider"></div>
              <p className="tertiary-dark text-center">Perfect for Schools and Large Groups</p>
              <div className="break"></div>
              <div className="break"></div>
              <div className="break"></div>
              <div className="flexible center full-width">
                <button className="flexible center tertiary clickable">
                  <NavLink to="/pricing/silver"></NavLink>
                  Order Now
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="break"></div>
        <div className="break"></div>
        <h4 className="flexible half-width primary tertiary-dark">*Garment charges are aside from the cost of labor. There is a $40 first time setup fee per color in design. Multiple designs per garment includes an extra charge per ink used.</h4>
        <div className="break"></div>
        <div className="break"></div>
      </div>
    </>
  )
}

export default Pricing;
