import React from 'react';
import { NavLink } from 'react-router-dom';
import Socials from './Socials';
import { MdFileUpload } from 'react-icons/md'
import { BsCheckCircle } from 'react-icons/bs';

import '../styles/Pricing.css'

function PricingBasic() {
  const [sec, setSec] = React.useState(1);
  const [prev, setPrev] = React.useState('');

  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [quantity, setQuantity] = React.useState('');
  const [colors, setColors] = React.useState(0);
  const [fileSet, setFileSet] = React.useState('');
  const [garment, setGarment] = React.useState('');

  const colorsAva = ['#FFF', '#000', '#d4d4d6', '#8a1738', '#1f2b44', '#94da49', '#d50332', '#1e4f90', '#ccff41', '#fe5004', '#7ca8d7']
  const [color, setColor] = React.useState('');

  const [small, setSmall] = React.useState('');
  const [medium, setMedium] = React.useState('');
  const [large, setLarge] = React.useState('');
  const [xlarge, setXLarge] = React.useState('');
  const [xxlarge, setXXLarge] = React.useState('');

  const [mockup, setMockup] = React.useState('');

  React.useEffect(() => {
    if (prev === '') return;

    setSec(2);
  }, [prev])

  const GetPrice = () => {
    let parsed = parseInt(colors);

    if (parsed === 4) {
      return 1.5;
    } else if (parsed === 3) {
      return 1;
    } else if (parsed === 2) {
      return 0.5;
    } else if (parsed === 1) {
      return 0;
    }
  }

  const GetTotal = () => {
    let total = GetLaborCost()

    if (garment === '5400') {
      total += Math.round(parseInt(quantity) * 4.50 * 100) / 100
    } else if (garment === '5000') {
      total += Math.round(parseInt(quantity) * 2.10 * 100) / 100
    } else if (garment === '5000L') {
      total += Math.round(parseInt(quantity) * 2.75 * 100) / 100
    }

    console.log(total);

    return total;
  }

  const GetSizesLeft = () => {
    return (parseInt(quantity) || 100) - (parseInt(small) || 0) - (parseInt(medium) || 0) - (parseInt(large) || 0) - (parseInt(xlarge) || 0) - (parseInt(xxlarge) || 0);
  }

  const GetLaborCost = () => {
    let labor = 0;

    labor += parseInt(quantity) * 2.5
    labor += prev === 'no' ? parseInt(colors) * 40 : parseInt(colors) * 20;
    labor += parseInt(quantity) * GetPrice();

    return labor;
  }

  return (
    <>
      <div id="pricing-ques" className="twntyfv-height flexible full-width center columns">
        <div className="break"></div>
        <div className="break"></div>
        <h4 className="flexible center full-width primary tertiary-dark">Just A Bit More Info</h4>
        <div className="break"></div>
        {
          sec === 1 &&
          <>
            <h1 className="flexible center primary text-center">Have you ordered with us before?</h1>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center rows">
              <button onClick={() => setPrev('no')} className="flexible clickable primary center danger">No</button>
              <button onClick={() => setPrev('yes')} className="flexible clickable tertiary center">Yes</button>
            </div>
          </>
        }
        {
          sec === 2 &&
          <>
            <h1 className="flexible center primary text-center">About You</h1>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <input type="text" name="name" onChange={(e) => setName(e.target.value)} placeholder="Name"/>
            </div>
            <div className="flexible center">
              <input type="tel" name="phoneNumber" onChange={(e) => setPhone(e.target.value)} placeholder="Phone Number"/>
            </div>
            <div className="flexible center">
              <input type="email" name="email" onChange={(e) => setEmail(e.target.value)} placeholder="E-Mail"/>
            </div>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={name === "" || phone === ''} onClick={() => setSec(3)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec === 3 &&
          <>
            <h1 className="flexible center primary text-center">How Many Do You Want?</h1>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <input type="number" name="quantity" onChange={(e) => setQuantity(e.target.value)} placeholder="Quantity" min="0"/>
            </div>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={quantity === "" || parseInt(quantity) < 24} onClick={() => setSec(4)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec === 4 &&
          <>
            <h1 className="flexible center primary text-center">About Your Design</h1>
            <div className="break"></div>
            <div className="break"></div>
            <h4 className="primary flexible rows center radio-h wrap">Quantity of Colors in Design</h4>
            <div className="flexible wrap columns">
              <p className="radio-cont">
                <input type="radio" id="one-color" name="colors" value="1" onChange={(e) => setColors(1)} />
                <label for="one-color">1 Color</label>
              </p>
              <p className="radio-cont">
                <input type="radio" id="two-color" name="colors" value="2" onChange={(e) => setColors(2)} />
                <label for="two-color">2 Colors (+ $0.50 per shirt)</label>
              </p>
              <p className="radio-cont">
                <input type="radio" id="t-color" name="colors" value="3" onChange={(e) => setColors(3)} />
                <label for="t-color">3 Colors (+ $1.00 per shirt)</label>
              </p>
              <p className="radio-cont">
                <input type="radio" id="f-color" name="colors" value="4" onChange={(e) => setColors(4)} />
                <label for="f-color">4 Colors (+ $1.50 per shirt)</label>
              </p>
            </div>
            <div className="break"></div>
            <div className="flexible center f-con">
              {
                fileSet !== "" &&
                <BsCheckCircle/>
              }
              {
                fileSet === "" &&
                <div className="file-input flexible clickable center rows">
                  <MdFileUpload/>
                  &nbsp;
                  Artwork File (eps, pdf, svg)
                  &nbsp;
                  <MdFileUpload/>
                </div>
              }
              <input type="file" accept=".eps,.ai,.svg,.pdf" name="artwork" onChange={(e) => setFileSet(e.target.files)} placeholder="Artwork File (eps, pdf, svg)"/>
            </div>
            <div className="break"></div>
            <div className="flexible center f-con columns">
              {
                mockup !== "" &&
                <BsCheckCircle/>
              }
              {
                mockup === "" &&
                <div className="file-input flexible clickable center rows">
                  <MdFileUpload/>
                  &nbsp;
                  Mockup File (png, jpg, jpeg)
                  &nbsp;
                  <MdFileUpload/>
                </div>
              }
              <input type="file" accept=".png, .jpg, .jpeg" name="artwork" onChange={(e) => setMockup(e.target.files)} placeholder="Mockup File (png, jpg, jpeg)"/>
            </div>
            <div className="break"></div>
            <h4 className="primary flexible rows center radio-h">If you don't have a mockup  file, create your design above!</h4>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={name === "" || phone === '' || !fileSet || !mockup || colors === 0} onClick={() => setSec(5)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec === 5 &&
          <>
            <h1 className="flexible center primary text-center">About Your Print</h1>
            <div className="break"></div>
            <div className="break"></div>
            <h4 className="primary flexible rows center radio-h">Garment for Your Print</h4>
            <div className="flexible columns">
              <p className="radio-cont">
                <input type="radio" id="gt-ls" name="garment-type" value="5400" onChange={(e) => setGarment('5400')} />
                <label for="gt-ls">5400 Gildan Heavy Cotton Long Sleeve</label>
              </p>
              <p className="radio-cont">
                <input type="radio" id="gt-hc" name="garment-type" value="5000" onChange={(e) => setGarment('5000')} />
                <label for="gt-hc">5000 Gildan Heavy Cotton</label>
              </p>
              <p className="radio-cont">
                <input type="radio" id="gt-ws" name="garment-type" value="womans" onChange={(e) => setGarment('5000L')} />
                <label for="gt-ws">5000L Gildan Heavy Cotton Women's</label>
              </p>
            </div>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={garment === ""} onClick={() => setSec(6000)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec > 5000 &&
          <>
            <h1 className="flexible center primary text-center">Let's talk about sizes</h1>
            <div className="break"></div>
            <h3 className="flexible center primary text-center">Shirt's Left: {GetSizesLeft()}</h3>
            <div className="break"></div>
            <div className="flexible center">
              <input type="number" name="small" onChange={(e) => setSmall(e.target.value)} placeholder="How many S?"/>
            </div>
            <div className="flexible center">
              <input type="number" name="medium" onChange={(e) => setMedium(e.target.value)} placeholder="How many M?"/>
            </div>
            <div className="flexible center">
              <input type="number" name="large" onChange={(e) => setLarge(e.target.value)} placeholder="How many L?"/>
            </div>
            <div className="flexible center">
              <input type="number" name="xlarge" onChange={(e) => setXLarge(e.target.value)} placeholder="How many XL?"/>
            </div>
            <div className="flexible center">
              <input type="number" name="xxlarge" onChange={(e) => setXXLarge(e.target.value)} placeholder="How many XXL?"/>
            </div>
            <h4 className="flexible center primary text-center">Note: For every XXL shirt there is a $1 charge</h4>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={GetSizesLeft() > 0} onClick={() => setSec(7)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec === 7 &&
          <>
            <h1 className="flexible center primary text-center">Choose Garment Color</h1>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center v-cs-cont wrap">
              {
                colorsAva.map(c => {
                  return (
                    <div onClick={() => setColor(c)} className={"v-c-cont hoverable popup clickable " + (c === color ? 'popped' : '')} style={{background: c}}></div>
                  )
                })
              }
            </div>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center">
              <button disabled={color === ""} onClick={() => setSec(8)} className="flexible center tertiary clickable">Next</button>
            </div>
          </>
        }
        {
          sec === 8 &&
          <>
            <h1 className="flexible center primary text-center">Estimated Cost</h1>
            <div className="break"></div>
            <div className="break"></div>
            <h3 className="pr primary flexible rows">Labor Charge x {quantity} Shirts: <div className="spacer"></div>${GetLaborCost()}</h3>
            {
              garment === '5000' &&
              <>
                <div className="break"></div>
                <h3 className="pr primary flexible rows">Garment Charge x {quantity} Shirts: <div className="spacer"></div>${Math.round(parseInt(quantity) * 2.10 * 100) / 100}</h3>
              </>
            }
            {
              garment === '5000L' &&
              <>
                <div className="break"></div>
                <h3 className="pr primary flexible rows">Garment Charge x {quantity} Shirts: <div className="spacer"></div>${Math.round(parseInt(quantity) * 2.75 * 100) / 100}</h3>
              </>
            }
            {
              garment === '5400' &&
              <>
                <div className="break"></div>
                <h3 className="pr primary flexible rows">Garment Charge x {quantity} Shirts: <div className="spacer"></div>${Math.round(parseInt(quantity) * 4.50 * 100) / 100}</h3>
              </>
            }
            {
              xxlarge !== "0" &&
              <>
                <div className="break"></div>
                <h3 className="pr primary flexible rows">Size Charge x {parseInt(xxlarge)} Shirts: <div className="spacer"></div>${parseInt(xxlarge) * 1}</h3>
              </>
            }
            <div className="divider sm"></div>
            <div className="break"></div>
            <h3 className="pr primary flexible rows">Estimated Total: <div className="spacer"></div>${GetTotal()}</h3>
            <div className="break"></div>
            <div className="break"></div>
            <form action="https://formspree.io/f/mvovnjke" enctype="multipart/form-data" method="post">
              <input type="hidden" name="name" value={name} />
              <input type="hidden" name="phone" value={phone} />
              <input type="hidden" name="email" value={email} />
              <input type="hidden" name="garment" value={garment} />
              <input type="hidden" name="quantity" value={quantity} />
              <input type="hidden" name="colors" value={colors} />
              <input type="hidden" accept=".pdf,.eps,.svg,.ai"  name="file" value={fileSet} />
              <input type="hidden" accept=".png,.jpeg,.jpg"  name="file" value={mockup} />
              <input type="hidden" name="total" value={GetTotal().toString()} />
              <input type="hidden" name="color" value={color} />
              <input type="hidden" name="small" value={small} />
              <input type="hidden" name="medium" value={medium} />
              <input type="hidden" name="large" value={large} />
              <input type="hidden" name="xlarge" value={xlarge} />
              <input type="hidden" name="xxlarge" value={xxlarge} />

              <div className="flexible center">
                <button type="button" onClick={() => setSec(2)} className="flexible center primary tertiaryFill clickable">Back</button>
                <button type="submit" onClick={() => setSec(9)} className="flexible center tertiary clickable">Order</button>
              </div>
            </form>
          </>
        }
        {
          sec === 9 &&
          <>
            <h1 className="flexible center primary text-center">Thank You!</h1>
            <div className="break"></div>
            <div className="break"></div>
            <h3 className="flexible center primary text-center">You will be contacted in 1-2 business days to follow up with your order</h3>
            <div className="break"></div>
            <h4 className="flexible center primary text-center">If you have any underlying questions, give us a call or send us an email so we can follow up with your questions or concerns.</h4>
            <div className="break"></div>
          </>
        }
        <div className="break"></div>
        <div className="break"></div>
      </div>
    </>
  )
}

export default PricingBasic;
