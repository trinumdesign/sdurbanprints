import React from 'react';
import { FaFacebookF, FaInstagram, FaBehance, FaEtsy } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMap, FiClock } from 'react-icons/fi'
import { IoCall, IoMail } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';

import '../styles/Footer.css'

function Footer() {
  return (
    <>
      <section id="footer" className="footer full-width flexible center rows wrap">
        <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_512/v1612312535/sd-urbanprints/iconion.png" alt="iconion" className="iconion"/>
        <div className="flexible columns toe">
          <h2 className="primary">About</h2>
          <div className="break"></div>
          <p className="primary opaque">We are a San Diego, California based screen printing company with a passion for what we do, here to meet all your needs!</p>
        </div>
        <div className="flexible columns toe">
          <h2 className="primary">Social Media</h2>
          <div className="break"></div>
          <a href="https://www.facebook.com/sdurbanprints" className="primary flexible rows"><FaFacebookF /><span className="opaque">Facebook</span></a>
          <a href="https://www.instagram.com/sdurbanprints" className="primary flexible rows"><FaInstagram /><span className="opaque">Instagram</span></a>
          <a className="primary flexible rows"><FaEtsy /><span className="opaque">Etsy</span></a>
        </div>
        <div className="flexible columns toe">
          <h2 className="primary">Contact</h2>
          <div className="break"></div>
          <a href="tel:6193723153" className="primary flexible rows"><IoMail/><span className="opaque">(619) 372-3153</span></a>
          <a href="mailto:sdurbanprints@yahoo.com" className="primary flexible rows"><IoCall/><span className="opaque">sdurbanprints@yahoo.com</span></a>
          <p className="primary flexible rows"><FiClock/><span className="opaque">Mon - Sun, 24/7</span></p>
        </div>
        <h5 className="copyright-creds full-width center tertiary flexible">© 2021 San Diego Urban Prints. All Rights Reserved. Developed by<a className="creds-ref" href="https://www.trinumdesign.com">&nbsp;Trinum Design Inc.</a></h5>
      </section>
    </>
  )
}

export default Footer;
