import React from 'react';
import '../styles/Varieties.css'
import { IoMdArrowRoundForward, IoMdCheckmark } from 'react-icons/io'
import { IoCall } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';
import { CgInfinity } from 'react-icons/cg';
import { FaTshirt } from 'react-icons/fa';
import { MdInvertColors } from 'react-icons/md';

function Varieties() {
    const [service, setService] = React.useState('');

    const colors = ['#FFF', '#000', '#d4d4d6', '#8a1738', '#1f2b44', '#94da49', '#d50332', '#1e4f90', '#ccff41', '#fe5004', '#7ca8d7']

    React.useEffect(() => {
      if (service != '') {
        document.getElementById('qs-header-start').scrollIntoView({behavior: 'smooth'});
      }
    }, [service])

    return (
      <>
        <div id="varities" className="twentyfv-height v-cont flexible full-width columns">
          <div className="break"></div>
          <div className="break"></div>
          <h4 className="flexible center full-width primary">Our Varities</h4>
          <div className="break"></div>
          <h1 className="flexible center tertiary-dark text-center">We Offer a Plethora of Options</h1>
          <h3 className="flexible center sm-top tertiary-dark opaque text-center">Select Item for More Info</h3>
          <div className="flexible center rows full-width wrap">
            <div onClick={() => setService('color')} className="flexible columns center srvcs-card clickable hoverable">
              <MdInvertColors/>
              <h3 className="bold flexible full-width center tertiary-dark">Color Choices</h3>
            </div>
            <div onClick={() => setService('quality')} className="flexible columns center srvcs-card clickable hoverable">
              <FaTshirt />
              <h3 className="bold flexible full-width center tertiary-dark">Garment Quality</h3>
            </div>
            <div onClick={() => setService('pricing')} className="flexible columns center srvcs-card clickable hoverable">
              <CgInfinity/>
              <h3 className="bold flexible full-width center tertiary-dark">Flexible Pricing</h3>
            </div>
          </div>
          {
            service != "" &&
            <div className="qs-c-a">
              <div className="break"></div>
              <div className="break"></div>
              <div className="qs-header-start">
                <div id="qs-header-start"></div>
              </div>
              {
                service == 'color' &&
                <>
                  <h1 className="primary full-width viga text-center qs-a-h">Our Available Colors</h1>
                  <div className="break"></div>
                  <div className="break"></div>
                  <div className="flexible center v-cs-cont wrap">
                    {
                      colors.map(color => {
                        return (
                          <div className="v-c-cont hoverable popup" style={{background: color}}></div>
                        )
                      })
                    }
                  </div>
                  <h1 className="primary full-width viga text-center">And Many More...</h1>
                </>
              }
              {
                service == 'quality' &&
                <>
                  <h1 className="primary full-width viga text-center qs-a-h">Our Garment Quality</h1>
                  <div className="break"></div>
                  <div className="break"></div>
                  <div className="flexible center v-cs-cont wrap">
                    <div className="flexible columns center">
                      <div className="v-q-cont zoom popup">
                        <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1612908316/sd-urbanprints/img-50-M_kynah5.jpg" alt="snap"/>
                      </div>
                      <h3 className="v-q-c-h primary flexible center text-center">5000 Gildan Heavy Cotton</h3>
                      <div className="break"></div>
                      <h5 className="v-q-c-t primary flexible center text-center">Seamless double-needle 7/8" collar. Taped neck and shoulders. Double-needle sleeve and bottom hems. Quarter-turned to eliminate center crease. Classic fit. Tear away label.</h5>
                      <div className="break"></div>
                      <h3 className="v-q-c-h primary flexible center text-center">Starts at $2.10 up to XL</h3>
                    </div>
                    <div className="flexible columns center">
                      <div className="v-q-cont zoom popup">
                        <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1612908316/sd-urbanprints/img-00-M_ntlhj8.jpg" alt="snap"/>
                      </div>
                      <h3 className="v-q-c-h primary flexible center text-center">5400 Gildan HCotton Long Sleeve</h3>
                      <div className="break"></div>
                      <h5 className="v-q-c-t primary flexible center text-center">5.3 oz., 100% cotton pre-shrunk jersey. Sport Grey is 90/10 cotton/polyester. Ash Grey is 99/1. Heathers are 50/50. Fiber content varies by color. Classic fit. Tear away label.</h5>
                      <div className="break"></div>
                      <h3 className="v-q-c-h primary flexible center text-center">Starts at $4.50 up to XL</h3>
                    </div>
                    <div className="flexible columns center">
                      <div className="v-q-cont zoom popup">
                        <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1612908316/sd-urbanprints/img-56-M_xurfvv.jpg" alt="snap"/>
                      </div>
                      <h3 className="v-q-c-h primary flexible center text-center">5000L Gildan HCotton Women's</h3>
                      <div className="break"></div>
                      <h5 className="v-q-c-t primary flexible center text-center">Semi-fitted contoured silhouette with side-seams for the perfect fit. Taped neck and shoulders. 1/2" mitered v-neck collar. Semi-fitted contoured silhouette with side seam.</h5>
                      <div className="break"></div>
                      <h3 className="v-q-c-h primary flexible center text-center">Starts at $2.75 up to XL</h3>
                    </div>
                  </div>
                </>
              }
              {
                service == 'pricing' &&
                <>
                  <div className="flexible rows v-p-cont">
                    <div className="flexible columns">
                      <h1 className="primary full-width viga text-center">Flexibility</h1>
                      <div className="break"></div>
                      <div className="break"></div>
                      <h2 className="flexible primary">Our prices vary depending on quantity, quality, and color choices!</h2>
                      <div className="break"></div>
                      <div className="break"></div>
                      <h4 className="primary full-width text-center">We aim to give fair pricing to all of our customers, so as to not charge them for something they do not purchase! We make it cost efficient for our partners by catering our prices to your specific choices!</h4>
                      <div className="break"></div>
                      <div className="break"></div>
                      <div className="flexible full-width center">
                        <button className="flexible tertiaryFill primary">
                          <NavLink to="/pricing"></NavLink>
                          Learn More
                        </button>
                      </div>
                    </div>
                  </div>
                </>
              }
              <div className="break"></div>
              <div className="break"></div>
            </div>
          }
        </div>
      </>
    )
}

export default Varieties;
