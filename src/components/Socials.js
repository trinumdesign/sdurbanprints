import React from 'react';
import '../styles/Socials.css';

import { FaFacebookF, FaInstagram } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMenu } from 'react-icons/fi'
import { HiLocationMarker } from 'react-icons/hi'
import { IoCall, IoMail } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';

function Socials({}) {
  return (
    <>
      <div className="socials-side flexible row">
        <div className="spacer"></div>
        <div className="flexible clickable center rows content">
          <div className="ico-cont flexible center">
            <IoMail className="tertiary" />
            <a target="_blank" href="mailto:sdurbanprints@yahoo.com"></a>
          </div>
          <div className="ico-cont flexible center">
            <IoCall className="tertiary" />
            <a target="_blank" href="tel:6193723153"></a>
          </div>
          <div className="ico-cont flexible center">
            <FaFacebookF className="tertiary" />
            <a target="_blank" href="https://www.facebook.com/sdurbanprints"></a>
          </div>
        </div>
        <div className="spacer5"></div>
      </div>
    </>
  )
}

export default Socials;
