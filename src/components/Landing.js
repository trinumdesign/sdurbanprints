import { NavLink } from 'react-router-dom';
import Socials from './Socials';

import '../styles/Landing.css'

function Landing() {
  return (
    <>
      <div id="landing" className="naityfv-height has-bg flexible full-width center columns">
        <div className="shadow"></div>
        <nav className={"flexible center elevate wrap full-width"}>
          <Socials />
          <div className="spacer5"></div>
          <NavLink to="/">
            <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_256/sd-urbanprints/iconion_nbg.png" alt="iconion" className="iconion"/>
          </NavLink>
          <div className="spacer"></div>
          <NavLink to="/varieties"  activeClassName="active" className="nav-tab primary bold flexible center uppercase">Varieties</NavLink>
          <NavLink to="/pricing"  activeClassName="active" className="nav-tab primary bold flexible center uppercase">Pricing</NavLink>
          <NavLink to="/builder"  activeClassName="active" className="nav-tab primary bold flexible center uppercase">Studio</NavLink>
          <NavLink to="/gallery"  activeClassName="active" className="nav-tab primary bold flexible center uppercase">Gallery</NavLink>
          <NavLink to="/contact"  activeClassName="active" className="nav-tab primary bold flexible center uppercase">Contact</NavLink>
          <div className="spacer5"></div>
        </nav>
        <div className="spacer flexible columns land-cont">
          <div className="spacer"></div>
          <h3 className="viga primary landing-header anim">Screenprints With<div className="break"></div><span className="bigger">Style</span></h3>
          <div className="break"></div>
          <div className="break"></div>
          <h4 className="primary">Cost effective prints at an amazing quality, perfect for your business, your brand, and your family festivies</h4>
          <div className="break"></div>
          <div className="break"></div>
          <div className="break"></div>
          <div className="btn-cont">
            <button className="flexible center hoverable clickable tertiary viga uppercase">
            <NavLink to="/pricing"></NavLink>
            Order Now
            </button>
          </div>
          <div className="spacer"></div>
        </div>
      </div>
    </>
  )
}

export default Landing;
