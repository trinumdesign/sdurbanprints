import React from 'react';
import {Image, Video, Transformation, CloudinaryContext} from 'cloudinary-react';
import { NavLink } from 'react-router-dom';
import Socials from './Socials';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import { IoCall } from 'react-icons/io5'
import { GiSaveArrow } from 'react-icons/gi'
import { BiPurchaseTag } from 'react-icons/bi';

import '../styles/Builder.css'

var cloudinary = require("cloudinary-core");

function Builder() {
  const [uploadID, setUploadID] = React.useState('');
  const [printarea, setPrintArea] = React.useState('');
  const [printzone, setPrintZone] = React.useState('');

  const props = {
    name: 'file',
    multiple: false,
    showUploadList: false,
    action: 'https://api.cloudinary.com/v1_1/trinum-daniel/upload',
    data: {
      api_key: '613617124989368',
      upload_preset: "qaiqr41t"
    },
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        console.log(info);
        setUploadID(info.file.response.public_id);
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const GetMetrics = () => {
    if (printzone === 'front') {
      return 'w_320,x_240,y_192,h_393,c_pad,g_north_west';
    } else if (printzone === 'cfront') {
      return 'w_213,x_293,y_212,h_213,c_pad,g_north_west';
    } else if (printzone === 'mfront') {
      return 'w_133,x_333,y_212,h_133,c_pad,g_north_west';
    } else if (printzone === 'lchest') {
      return 'w_133,x_427,y_192,h_133,c_pad,g_north_west';
    } else if (printzone === 'back') {
      return 'w_320,x_240,y_131,h_450,c_pad,g_north_west';
    } else if (printzone === 'uback') {
      return 'w_320,x_240,y_131,h_106,c_pad,g_north_west';
    } else if (printzone === 'bback') {
      return 'w_320,x_240,y_579,h_106,c_pad,g_north_west';
    }
  }

  const FixUpload = () => {
    return uploadID.replace(/\//g, ":");
  }

  const GetMock = () => {
    let  frontzone = ['front', 'rchest', 'lchest', 'cfront', 'mfront'];
    return frontzone.includes(printzone) ? 't-shirt_cecxhi' : 't-shirt_back_ien7nn'
  }

  return (
    <>
      <div id="builder" className="svnty-height primaryFill flexible full-width center columns">
        <div className="break"></div>
        <div className="break"></div>
        <h4 className="flexible center full-width primary tertiary">Design Builder</h4>
        <div className="break"></div>
        <h1 className="flexible center tertiary text-center">Try Out Our Mockup Builder</h1>
        <div className="break"></div>
        <div className="break"></div>
        {
          uploadID == "" &&
          <>
            <h4 className="flexible center full-width primary tertiary">Start by uploading your design</h4>
            <div className="break"></div>
            <Upload {...props}>
              <button className="popup flexible center rows clickable tertiary"><UploadOutlined />&nbsp;&nbsp;Click to Upload</button>
            </Upload>
          </>
        }
        {
          uploadID !== "" && printarea === "" &&
          <>
            <h4 className="flexible center full-width primary tertiary">Choose Print Zone</h4>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center v-cs-cont rows wrap">
              <div onClick={() => setPrintArea('front')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4/sd-urbanprints/mockups/t-shirt_layers_x75umt.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Front</h3>
              </div>

              <div onClick={() => setPrintZone('back')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4/sd-urbanprints/mockups/t-shirt_back_layers_fpahot.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Back</h3>
              </div>
            </div>
          </>
        }
        {
          uploadID !== "" && printarea === "front" && printzone == "" &&
          <>
            <h4 className="flexible center full-width primary tertiary">Choose Print Zone</h4>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center v-cs-cont rows wrap">
              <div onClick={() => setPrintZone('front')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;5/sd-urbanprints/mockups/t-shirt_layers_x75umt.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Front (12in x 18in)</h3>
              </div>

              <div onClick={() => setPrintZone('cfront')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;6/sd-urbanprints/mockups/t-shirt_layers_x75umt.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Center Front (10in x 10in)</h3>
              </div>

              <div onClick={() => setPrintZone('mfront')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;6/sd-urbanprints/mockups/t-shirt_layers_x75umt.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Center Front (7in x 7in)</h3>
              </div>

              <div onClick={() => setPrintZone('lchest')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;9/sd-urbanprints/mockups/t-shirt_layers_x75umt.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Left Chest (4in x 4in)</h3>
              </div>
            </div>
          </>
        }
        {
          uploadID !== "" && printarea === "back" && printzone == "" &&
          <>
            <h4 className="flexible center full-width primary tertiary">Choose Print Zone</h4>
            <div className="break"></div>
            <div className="break"></div>
            <div className="flexible center v-cs-cont rows wrap">
              <div onClick={() => setPrintZone('back')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;5/sd-urbanprints/mockups/t-shirt_back_layers_fpahot.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Back (12in x 18in)</h3>
              </div>

              <div onClick={() => setPrintZone('uback')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;8/sd-urbanprints/mockups/t-shirt_back_layers_fpahot.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Upper Back (12in x 5in)</h3>
              </div>

              <div onClick={() => setPrintZone('bback')} className="flexible columns center clickable">
                <div className="v-q-cont b zoom popup">
                  <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_400/pg_4;9/sd-urbanprints/mockups/t-shirt_back_layers_fpahot.png" alt="snap"/>
                </div>
                <h3 className="v-q-c-h tertiary flexible center text-center">Lower Back (12in x 5in)</h3>
              </div>
            </div>
          </>
        }
        {
          uploadID !== "" && printzone !== "" &&
          <>
            <img src={`https://res.cloudinary.com/trinum-daniel/image/upload/l_${FixUpload(uploadID)},${GetMetrics()}/w_400/sd-urbanprints/mockups/${GetMock()}.png`} alt="snap"/>
            <div className="break"></div>
            <div className="flexible center rows full-width wrap">
              <a download href={`https://res.cloudinary.com/trinum-daniel/image/upload/l_${FixUpload(uploadID)},${GetMetrics()}/w_400/sd-urbanprints/mockups/${GetMock()}.png`} className="flexible columns center srvcs-card-sm clickable hoverable">
                <GiSaveArrow className="tertiary"/>
                <div className="break"></div>
                <h3 className="bold flexible full-width center tertiary">Save Image</h3>
              </a>
              <NavLink to="/pricing"  className="flexible columns center srvcs-card-sm clickable hoverable">
                <BiPurchaseTag className="tertiary"/>
                <div className="break"></div>
                <h3 className="bold flexible full-width center tertiary">Buy Now</h3>
              </NavLink>
              <a href="tel:6193723153" className="flexible columns center srvcs-card-sm clickable hoverable">
                <IoCall className="tertiary"/>
                <div className="break"></div>
                <h3 className="bold flexible full-width center tertiary">Contact Us</h3>
              </a>
            </div>
          </>
        }
        <div className="break"></div>
        <div className="break"></div>
      </div>
    </>
  )
}

export default Builder;
